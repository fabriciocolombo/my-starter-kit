This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

You can find the most recent version of the guide [here](https://github.com/facebookincubator/create-react-app/blob/master/packages/react-scripts/template/README.md).

## Take a look

- [Firebase Instance](https://my-starter-kit.firebaseapp.com)
- [Heroku Instance](https://my-starter-kit.herokuapp.com)

## Heroku

To create the Heroku App:

´´´
heroku create my-starter-kit -b https://github.com/mars/create-react-app-buildpack.git
´´´
