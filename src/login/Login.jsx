import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import LoaderButton from '../components/LoaderButton';
import './Login.css';

class Login extends Component {

  constructor(props) {
    super(props);

    this.state = {
      isLoading: false,
      username: '',
      password: '',
    };
  }

  validateForm() {
    return this.state.username.length > 0
      && this.state.password.length > 0;
  }

  login(username, password) {
    return new Promise((resolve, reject) => (
      setTimeout(() => {
        resolve(`${username}:${password}`)
      }, 2000)
    ));
  }

  handleChange = (event) => {
    this.setState({
      [event.target.id]: event.target.value
    });
  }

  handleSubmit = async (event) => {
    event.preventDefault();

    this.setState({ isLoading: true });

    try {
      const userToken = await this.login(this.state.username, this.state.password);

      this.props.login(userToken);
    }
    catch(e) {
      alert(e);
      this.setState({ isLoading: false });
    }
  }

  render() {
    return (
      <div className="Login">
        <form onSubmit={this.handleSubmit}>
          <div className="row">
            <div className="input-field col s12">
              <input id="username" type="text" className="validate" onChange={this.handleChange}/>
              <label for="username">Login</label>
            </div>
          </div>
          <div className="row">
            <div className="input-field col s12">
              <input id="password" type="password" className="validate" onChange={this.handleChange}/>
              <label for="password">Senha</label>
            </div>
          </div>
          <LoaderButton
            block
            bsSize="large"
            disabled={ ! this.validateForm() }
            type="submit"
            isLoading={this.state.isLoading}
            text="Login"/>

        </form>
      </div>
    );
  }
}

export default withRouter(Login);
