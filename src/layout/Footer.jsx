import React from 'react';

export default function Footer({ logo }){
  return (
    <footer className="page-footer">
      <div className="footer-copyright">
        <div className="container">
          © 2017 My Starter Kit
        </div>
      </div>
    </footer>
  );
}
