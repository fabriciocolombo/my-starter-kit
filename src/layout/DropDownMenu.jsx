import React from 'react';
import { Link } from 'react-router-dom';

export default function DropDownMenu({ logout }) {
  return (
    <ul id="dropdown1" className="dropdown-content" style={{width: '200px'}}>
      <li>
        <Link title="Minha Conta" to="/user" >
          <i className="material-icons">account_box</i>
          Minha Conta
        </Link>
      </li>
      <li><a href="#!"><i className="material-icons">view_module</i>four</a></li>
      <li className="divider"></li>
      <li>
        <Link title="Sair" to="/logout" onClick={logout} >
          <i className="material-icons">exit_to_app</i>
          Sair
        </Link>
      </li>
    </ul>
  );
}
