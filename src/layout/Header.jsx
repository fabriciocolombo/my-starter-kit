import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Badge from '../components/Badge.jsx';
import NavItem from '../components/NavItem.jsx';
import LoginButton from '../components/LoginButton.jsx';
import Search from '../components/Search.jsx';
import DropDownMenu from './DropDownMenu.jsx';

export default class Header extends Component {

  constructor(props) {
    super(props);
    this.state = {
      searchActive: false
    };
  }

  componentDidMount() {
    this.activateDropDown();
  }

  componentWillReceiveProps() {
    this.activateDropDown();
  }

  activateDropDown = () => {
    window.$(".dropdown-button").dropdown({
      constrainWidth: false,
      belowOrigin: true
    });
  }

  onClickSearch = event => {

    this.setState({
      searchActive: !this.state.searchActive
    });
  }

  render() {
    const { user, logout } = this.props;
    const { searchActive } = this.state;
    const isLogged = user && user.userToken;

    return (
      <header>

        <DropDownMenu logout={logout}/>

        <div className="navbar fixed">
          <nav>
            {searchActive &&
              <Search onClose={this.onClickSearch} />
            }
            <div hidden={searchActive} className="nav-wrapper" >
              <a href="#/" data-activates="slide-out" className="button-collapse"><i className="material-icons">menu</i></a>
              <Link to="/" className="brand-logo" >My Starter Kit</Link>
              <ul className="right hide-on-med-and-down">
                <NavItem icon="search" title="Pesquisa" onClick={this.onClickSearch}/>
                <NavItem icon="apps" title="Módulos"/>
                <NavItem icon="notifications" title="Notificações">
                  <Badge value={+99} caption="Notificações" circle/>
                </NavItem>
                <NavItem icon="refresh" title="Atualizar"/>

                <NavItem hidden={!isLogged} className="dropdown-button" data-activates="dropdown1" icon="account_box" title="Minha Conta">

                </NavItem>

                {!isLogged  &&
                  <LoginButton />
                }
              </ul>
            </div>
          </nav>
        </div>
      </header>
    );
  }
}
