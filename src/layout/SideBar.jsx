import React, { Component } from 'react';
import backgroundLogo from '../assets/images/office.jpg';
import PropTypes from 'prop-types';
import Icon from '../components/Icon.jsx';
import Badge from '../components/Badge.jsx';
import Divider from '../components/Divider.jsx';
import { Link } from 'react-router-dom';
import './SideBar.css';

export default class SideBar extends Component {

  constructor(props) {
    super(props);
    this.state = {
      collapsed: true
    };
  }

  static propTypes = {
    user: PropTypes.object.isRequired
  }

  componentDidMount() {
    // Initialize collapse button
    window.$(".button-collapse").sideNav();

    /*
    window.$(".button-collapse").sideNav({
        menuWidth: 300, // Default is 300
        edge: 'left', // Choose the horizontal origin
        closeOnClick: false, // Closes side-nav on <a> clicks, useful for Angular/Meteor
        draggable: true // Choose whether you can drag to open on touch screens
      }
    );
    */
    // Initialize collapsible (uncomment the line below if you use the dropdown variation)
    //$('.collapsible').collapsible();
  }

  render() {
    const { user } = this.props;

    return (
      <div>
        <ul id="slide-out" className="side-nav fixed" >
          <li>
            <div className="userView">
              <div className="background">
                <img src={backgroundLogo} alt="logo" />
              </div>
              <a href="/user"><img className="circle" src={user.avatar} alt="users" /></a>
              <span className="white-text name">{user.name}</span>
              <span className="white-text email">{user.email}</span>
            </div>
          </li>
          <li>
            <Link to="/inbox" className="waves-effect waves-teal">
              <Icon name="inbox" className="blue-text" />
              Entrada
              <Badge value="47" />
            </Link>
          </li>
          <li>
            <a href="/important" className="waves-effect waves-teal">
              <Icon name="announcement" className="orange-text" />
              Importante
              <Badge value="8" className="red"/>
            </a>
          </li>
          <li>
            <a href="/favorites" className="waves-effect waves-teal">
              <Icon name="start" className="yellow-text" />
              Favoritos
            </a>
          </li>
          <Divider />
          <li>
            <a href="/drafts" className="waves-effect waves-teal">
              <Icon name="drafts" />
              Rascunhos
            </a>
          </li>
          <li>
            <a href="/sent" className="waves-effect waves-teal">
              <Icon name="send" />
              Enviados
            </a>
          </li>
          <li>
            <a href="/recyclebin" className="waves-effect waves-teal">
              <Icon name="delete" />
              Lixeira
            </a>
          </li>
          <Divider />
          <li>
            <ul className="collapsible collapsible-accordion">
              <li className="bold active"><a className="collapsible-header waves-effect waves-teal active">Marcadores</a>
                <div className="collapsible-body" style={{display: 'block'}}>
                  <ul>
                    <li><a href="pagination.html"><Icon name="label" className="blue-text"/> Recursos Humanos</a></li>
                    <li><a href="pagination.html"><Icon name="label" className="red-text"/> Compromissos</a></li>
                  </ul>
                </div>
              </li>
            </ul>
          </li>
          <Divider />
          <li>
            <a href="/inbox" className="waves-effect waves-teal">
              <Icon name="settings" />
              Configurações
            </a>
          </li>
          <li>
            <a href="/inbox" className="waves-effect waves-teal">
              <Icon name="help" />
              Ajuda
            </a>
          </li>
        </ul>
      </div>
    );
  }
}
