import React from 'react';
import classnames from 'classnames';

const badgeCircleStyle = {
  minWidth: '0px',
  borderRadius: '100%',
  position: 'absolute',
  marginTop: '8px',
  marginLeft: '15px'
};

export default function Badge({ value, left, right, circle, className }) {
  const classes = classnames('new badge', className, {
    left: left,
    right: right
  });
  return (
    <span className={classes} data-badge-caption="" style={circle && badgeCircleStyle}>
      {value}
    </span>
  );
}
