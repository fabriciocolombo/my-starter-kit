import React, { Component } from 'react';
import './ScrollToTopButton.css';

class ScrollToTopButton extends Component {

  onClick = () => {
    window.scrollTo(0, 0)
  }

  render() {
    return (
      <div className="scroll-to-top">
        <div
          onClick={this.onClick}
          className="arrow-up"
        />      
      </div>
    );
  }
}

export default ScrollToTopButton;