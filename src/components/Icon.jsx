import React from 'react';
import classnames from 'classnames';

export default function Icon({ name, left, right, className}) {
  const classes = classnames('material-icons', className, {
    left: left,
    right: right
  });
  return (
    <i className={classes}>{name}</i>
  );
}
