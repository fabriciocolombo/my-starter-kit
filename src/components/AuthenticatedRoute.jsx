import React from 'react';
import { Route, Redirect } from 'react-router-dom';

const AuthenticatedRoute = ({ component: Component, childProps, ...rest }) => {

  const render = props => {
    if (childProps.userToken) {
      return <Component {...props} {...childProps} />
    }
    
    return <Redirect to={`/login?redirect=${props.location.pathname}${props.location.search}`} />
  }
  return (
    <Route {...rest} render={render} />
  );
}


export default AuthenticatedRoute;