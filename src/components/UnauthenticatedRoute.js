import React from 'react';
import { Route, Redirect } from 'react-router-dom';

function querystring(name, url = window.location.href) {
  name = name.replace(/[[]]/g, "\\$&");

  const regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)", "i");
  const results = regex.exec(url);

  if ( ! results) { return null; }
  if ( ! results[2]) { return ''; }

  return decodeURIComponent(results[2].replace(/\+/g, " "));
}

const UnauthenticatedRoute = ({ component: Component, childProps, ...rest }) => {

  const redirect = querystring('redirect') || '/';

  const render = props => {
    if (childProps.userToken) {
      return <Redirect to={redirect} />
    }
    
    return <Component {...props} {...childProps} />    
  }
  return (
    <Route {...rest} render={render} />
  );
}

export default UnauthenticatedRoute;