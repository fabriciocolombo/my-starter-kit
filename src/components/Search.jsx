import React from 'react';

export default function Search({ onClose, ...props }){

  return (
    <div className="nav-wrapper" {...props} >
      <form>
        <div className="input-field">
          <input autoFocus id="search" type="search" required />
          <label className="label-icon" htmlFor="search"><i className="material-icons">search</i></label>
          <i className="material-icons" onClick={onClose}>close</i>
        </div>
      </form>
    </div>
  );

}
