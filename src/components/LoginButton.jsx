import React, { Component } from 'react';
import NavItem from './NavItem.jsx';

class LoginButton extends Component {

  render() {
    return (
      <NavItem icon="account_circle" title="Entrar" button to="/login"/>
    );
  }
}

export default LoginButton;
