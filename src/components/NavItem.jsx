import React from 'react';
import { Link } from 'react-router-dom';

export default function NavItem({ icon, title, onClick, children, button, to = "#/", ...props }) {

  return (
    <li >
      {button
        ?
          <Link className="btn" title={title} onClick={onClick} to={to} {...props} >
            {title}
            {children}
            <i className="material-icons right">{icon}</i>
          </Link>
        :
          <Link style={{height :'64px'}} title={title} onClick={onClick} to={to} {...props} >
            {children}
            <i className="material-icons">{icon}</i>
          </Link>
      }

    </li>
  );
}
