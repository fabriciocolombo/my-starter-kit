import React from 'react';

const loaderStyle = {
  width: '24px',
  height: '24px',
  top: '6px',
  marginRight: '15px'
};

export default function LoaderButton({ isLoading, text, loadingText = 'Carregando...', disabled = false, ...props }){
  return (
    <button className="waves-effect waves-light btn" disabled={ disabled || isLoading } {...props}>
      {isLoading &&
        <div className="preloader-wrapper small active" style={loaderStyle}>
          <div className="spinner-layer spinner-green-only">
            <div className="circle-clipper left">
              <div className="circle"></div>
            </div><div className="gap-patch">
              <div className="circle"></div>
            </div><div className="circle-clipper right">
              <div className="circle"></div>
            </div>
          </div>
        </div>
      }
      { ! isLoading ? text : loadingText }
    </button>
  );
}
