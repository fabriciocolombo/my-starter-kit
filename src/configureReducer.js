import { combineReducers } from 'redux';

import todos from './todos/reducer.js';

// stackoverflow.com/q/35622588/233902
const resetStateOnSignOutReducer = (reducer, initialState) => {
  return (state, action) => {
    const userWasSignedOut = action.type === 'LOGOUT_SUCCESS';

    if (!userWasSignedOut) {
      return reducer(state, action);
    }

    // Note how we can purge sensitive data without hard reload easily.
    let stateWithoutSensitiveData = {
      app: state.app,
      config: initialState.config
    };

    // Preserve router reducer
    stateWithoutSensitiveData = {
      ...stateWithoutSensitiveData,
      routing: state.routing
    };

    return reducer(stateWithoutSensitiveData, action);
  }
}

const configureReducer = (initialState = {}) => {

  const rootReducer = combineReducers({
    todos
  });

  // The power of higher-order reducers, http://slides.com/omnidan/hor
  return resetStateOnSignOutReducer(rootReducer, initialState);
};

export default configureReducer;