import React, { Component } from 'react';
import Routes from './Routes.js';
import logo from './logo.svg';
import Header from './layout/Header.jsx';
import SideBar from './layout/SideBar.jsx';
import Footer from './layout/Footer.jsx';
import './App.css';

class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      userToken: undefined
    };
  }

  login = (userToken) => {
    this.setState({
      userToken
    });
  }

  logout = () => {
    this.setState({
      userToken: undefined
    });
  }

  render() {
    const childProps = {
      userToken: this.state.userToken,
      login: this.login
    };

    const user = {
      avatar: 'http://materializecss.com/images/yuna.jpg',
      name: 'John Doe',
      email: 'jdandturk@gmail.com',
      userToken: this.state.userToken
    };

    return (
      <div className="App">
        <Header logo={logo} user={user} logout={this.logout} />

        <SideBar logo={logo} user={user}/>

        <main>
          <div className="container main-container">
            <Routes childProps={childProps}/>
          </div>
        </main>

        <div className="fixed-action-btn">
          <a className="btn-floating btn-large red">
            <i className="large material-icons">mode_edit</i>
          </a>
          <ul>
            <li><a className="btn-floating red"><i className="material-icons">insert_chart</i></a></li>
            <li><a className="btn-floating yellow darken-1"><i className="material-icons">format_quote</i></a></li>
            <li><a className="btn-floating green"><i className="material-icons">publish</i></a></li>
            <li><a className="btn-floating blue"><i className="material-icons">attach_file</i></a></li>
          </ul>
        </div>

        <Footer />
      </div>
    );
  }
}

export default App;
