import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Home from './home/Home.jsx';
import asyncComponent from './components/asyncComponent';
import AuthenticatedRoute from './components/AuthenticatedRoute.jsx';
import UnauthenticatedRoute from './components/UnauthenticatedRoute.js';

const importAbout = () => import('./about/About.jsx');
const importTodos = () => import('./todos/Page.jsx');
const importNotFound = () => import('./notFound/NotFound.jsx');
const importLogin = () => import('./login/Login.jsx');
const importInbox = () => import('./inbox/List.jsx');

const Routes = ({ childProps }) => {
  return (
    <Switch>
      <Route path="/" exact component={Home} />
      <Route path="/about" exact component={asyncComponent(importAbout)} />
      <AuthenticatedRoute path="/todos" exact component={asyncComponent(importTodos)} childProps={childProps} />
      <AuthenticatedRoute path="/inbox" exact component={asyncComponent(importInbox)} childProps={childProps} />
      <UnauthenticatedRoute path="/login" exact component={asyncComponent(importLogin)} childProps={childProps} />
      <Route component={asyncComponent(importNotFound)} />
    </Switch>
  );
}

export default Routes;
