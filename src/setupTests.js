const jQuery = require('jquery');

jQuery.fn.dropdown = () => {};
jQuery.fn.sideNav = () => {};

window.jQuery = jQuery;
window.$ = jQuery;

const localStorageMock = {
  getItem: jest.fn(),
  setItem: jest.fn(),
  clear: jest.fn()
};
global.localStorage = localStorageMock
