import React, { Component } from 'react';

export class Home extends Component {

  render() {
    return (
      <div>
        <h3>
          Current running instances:
        </h3>
        <ul>
          <li>
            <a href="https://my-starter-kit.firebaseapp.com">
              Firebase Instance
            </a>
          </li>
          <li>
            <a href="https://my-starter-kit.herokuapp.com">
              Heroku Instance
            </a>
          </li>
        </ul>
      </div>
    );
  }
}

export default Home;
