import React from 'react';
import ReactDOM from 'react-dom';
import registerServiceWorker from './registerServiceWorker';
import { BrowserRouter as Router } from 'react-router-dom';
import { Provider } from 'react-redux';
import App from './App.js';
import configureReducer from './configureReducer.js';
import configureStore from './configureStore.js';

import './index.css';

/** Materialize CSS */
import '../node_modules/materialize-css/dist/css/materialize.min.css';
import jQuery from '../node_modules/materialize-css/node_modules/jquery/dist/jquery.js';
window.jQuery = jQuery;
window.$ = jQuery;
require('../node_modules/materialize-css/dist/js/materialize.js');


const initialState = window.__INITIAL_STATE__ || {};
const rootReducer = configureReducer(initialState);
const store = configureStore(rootReducer);

ReactDOM.render(
  <Provider store={store}>
    <Router>
        <App />
    </Router>
  </Provider>,
  document.getElementById('root')
);
registerServiceWorker();
