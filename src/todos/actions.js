import {
  TODOS_FETCH_ALL
} from './types.js';

export function fetchAll() {
  return {
    type: TODOS_FETCH_ALL   
  }
};