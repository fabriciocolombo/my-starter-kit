import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import * as actions from './actions.js';

export class Todos extends Component {

  static propTypes = {
    fetchAll: PropTypes.func.isRequired
  }

  render() {
    const { fetchAll } = this.props;

    return (
        <form>
          <input type="text" label="teste" />
          <button type="buttton" onClick={fetchAll} >Add</button>
        </form>
    );
  }
}

const mapStateToProps = (state) => {
  return {

  };
};

export default connect(mapStateToProps, actions)(Todos);
