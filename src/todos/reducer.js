import {
  TODOS_FETCH_ALL_SUCCESS
} from './types.js';

const initialState = {
  todos: [],
  filter: {}
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case TODOS_FETCH_ALL_SUCCESS:
      return state;
    default:
      return state;
  }
}
