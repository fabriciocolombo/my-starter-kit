import React from 'react';
import { Link } from 'react-router-dom';

export default function InboxItem({ img, icon, title, children, id, className }) {

  return (
    <li className="collection-item avatar">
      <Link to={`/inbox/${id}`} className="black-text">
        {img && <img src={img} alt="" className="circle" />}
        {icon && <i className={`material-icons circle ${className}`}>{icon}</i>}
        <span className="title">{title}</span>
        {children}
        <a href="#!" className="secondary-content"><i className="material-icons">star</i></a>
      </Link>
    </li>
  );
}
