import React from 'react';
import Item from './InboxItem.jsx';

export default function Inbox() {

  return (
    <ul className="collection">
      <Item id={1} img="http://materializecss.com/images/yuna.jpg" title="First">
        <p>First Line <br/>
           Second Line
        </p>
      </Item>
      <Item id={2} icon="folder" title="Second">
        <p>First Line <br/>
           Second Line
        </p>
      </Item>
      <Item id={3} icon="insert_chart" title="Third" className="green">
        <p>First Line <br/>
           Second Line
        </p>
      </Item>
      <Item id={4} icon="grade" title="Fourth" className="red">
        <p>First Line <br/>
           Second Line
        </p>
      </Item>
    </ul>
  );
}
